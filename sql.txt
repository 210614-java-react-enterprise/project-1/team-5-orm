create table movie(
	media_id integer primary key,
	release_year integer,
	runtime integer,
	title varchar(100)
);

create table review(
	review_id integer primary key,
	user_id integer,
	media_id integer,
	review_text varchar(100),
	score integer,
	constraint fk_user
		foreign key(user_id)
			references review_user(user_id),
	constraint fk_movie
		foreign key(media_id)
			references movie(media_id)
);

create table review_user(
	user_id integer primary key,
	username varchar(100),
	user_password varchar(100)
);

drop table review;
drop table movie;
drop table review_user;
truncate table movie