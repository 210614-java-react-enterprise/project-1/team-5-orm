package dev.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionPoolUtil {
    private List<Connection> connectionPool;
    private List<Connection> usedConnections = new ArrayList<>();
    private static int size =10;

    public ConnectionPoolUtil(){
        connectionPool = new ArrayList<>(size);
        for(int i=0; i < size; i++){
            try {
                connectionPool.add(ConnectionUtil.getConnection());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public Connection getConnection(){
        Connection connection = connectionPool.remove(connectionPool.size() -1);
        usedConnections.add(connection);
        return connection;
    }

    public void releaseConnection(Connection connection){
        connectionPool.add(connection);
        usedConnections.remove(connection);
    }


}
