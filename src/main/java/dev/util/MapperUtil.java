package dev.util;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Locale;

public class MapperUtil {
    public static String convertJavaNameToSQLName(String className){
        StringBuffer s = new StringBuffer();
        String classNameLowered = className.substring(0,1).toLowerCase() + className.substring(1);
        for(char c: classNameLowered.toCharArray()){
            if(Character.isUpperCase(c)){
                s.append("_");
                s.append(Character.toLowerCase(c));
            }else{
                s.append(c);
            }
        }
        return s.toString();
    }

    public static String convertJQLNameToClassName(String className){
        String[] words = className.split("_");
        StringBuffer s = new StringBuffer();
        for(String word:  words ){
            String nameUpper = word.substring(0,1).toUpperCase() + word.substring(1);
            s.append(nameUpper);
        }
        return s.toString();
    }



    //can also use generic
    public static void setWhatever(PreparedStatement ps, int index, Object fieldValue, Class<?> type) throws InstantiationException, IllegalAccessException, SQLException {
        System.out.println("setWhatever: type.getName(): " + type.getName());
        switch(type.getName()){
            case "double":
                //System.out.println("double: " + fieldValue);
                ps.setDouble(index, (Double) fieldValue);
                break;
            case "int":
                //System.out.println("int: " + fieldValue);
                ps.setInt(index, (Integer) fieldValue);
                break;
            case "java.lang.Integer":
                //System.out.println("java.lang.Integer: " + fieldValue);
                ps.setInt(index, (Integer) fieldValue);
                break;
            case "boolean":
                //System.out.println("boolean\": " + fieldValue);
                ps.setBoolean(index, (Boolean) fieldValue);
                break;
            case "java.lang.String":
                //System.out.println("java.lang.String: " + fieldValue);
                ps.setString(index, String.valueOf(fieldValue));
                break;
//            case "java.time.LocalDate":
//                break;
        }
    }
    public static Object getWhatever(ResultSet rs, String columnName, Class<?> type) throws InstantiationException, IllegalAccessException, SQLException {
        Object fieldValue;
        switch(type.getName()){
            case "double":
                fieldValue = rs.getDouble(columnName);
                //System.out.println("double: " + fieldValue);
                return  fieldValue;
            case "int":
                fieldValue = rs.getInt(columnName);
                //System.out.println("Int: " + fieldValue);
                return  fieldValue;
            case "boolean":
                fieldValue = rs.getBoolean(columnName);
                //System.out.println("boolean\": " + fieldValue);
                return  fieldValue;
            case "java.lang.String":
                fieldValue = rs.getString(columnName);
                //System.out.println("java.lang.String: " + fieldValue);
                return  fieldValue;
//            case "java.time.LocalDate":
//                break;
        }
        return null;
    }
    public static Object convertStringToFieldType(String input, Class<?> type) throws IllegalAccessException, InstantiationException {
        switch(type.getName()){
            case "byte":
                return Byte.valueOf(input);
            case "short":
                return Short.valueOf(input);
            case "int":
                return Integer.valueOf(input);
            case "long":
                return Long.valueOf(input);
            case "boolean":
                return Boolean.valueOf(input);
            case "java.lang.String":
                return input;
            case "java.time.LocalDate":
                return LocalDate.parse(input);
            default:
                return type.newInstance();
        }
    }
}
