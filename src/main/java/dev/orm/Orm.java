package dev.orm;

import dev.util.ConnectionPoolUtil;
import dev.util.ConnectionUtil;
import dev.util.MapperUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Orm {

    private ConnectionPoolUtil connectionPool = new ConnectionPoolUtil();;


    public boolean add(Object o) {
        // object relational mapping
        // column in sql table should follow the same order as field in java class
        // need to implement: get, getAll, delete, modify
        // need to implement: setString or setBoolean according to field type
        Class<?> objectClass = o.getClass();
        String[] className = objectClass.toString().split("\\.");
        String tableName = className[className.length-1];
        System.out.println("tableName: " + tableName);
        Field[] fields = objectClass.getDeclaredFields();
        System.out.println("fields.length: " + fields.length);
        String s =  " values (?";
        for(int i=1; i< fields.length; i++){
            s = s + ",?";
        }
        s = s + ");";
        String sql = "insert into " + MapperUtil.convertJavaNameToSQLName(tableName) + s;
        Connection connection = connectionPool.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql);) {
            // iterate through each field --- Fields [id, name, birthday]
            int index = 1;
            for(Field field: fields) { //assumed order !!
                String fieldName = field.getName();

                //fieldName = MapperUtil.convertJavaNameToSQLName(fieldName); //!!!

                String getterName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                //  access the getter directly based on the field name
                Method getterMethod = o.getClass().getMethod(getterName);
                // if we invoke the getter, we get the field value
                Object fieldValue = getterMethod.invoke(o); //need to change
                //ps.setString(index, fieldValue)
                MapperUtil.setWhatever(ps, index, fieldValue, field.getType());

                index += 1;
            }
            System.out.println(ps);
            int success = ps.executeUpdate();
            if(success>0){
                connectionPool.releaseConnection(connection);
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        connectionPool.releaseConnection(connection);
        return false;


    }

    //reference to deSerialize method in JosnMapper
    public <T> List<T> get(Class<T>  clazz) {
        //System.out.println(clazz.getName());
        String[] words = clazz.getName().split("\\.");
        String tableName = words[words.length-1];
        String sql = "select * from " + MapperUtil.convertJavaNameToSQLName(tableName);
        Connection connection = connectionPool.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql);) {
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            List<T> objectList = new ArrayList<>();
            Field[] fields = clazz.getDeclaredFields();
            while(rs.next()){
                T newObject = null;
                try {
                    newObject = clazz.newInstance();
                } catch (InstantiationException |IllegalAccessException e) {
                    e.printStackTrace();
                }
                for(Field field: fields) { //assumed order !!
                    String fieldName = field.getName();
                    String columnName = MapperUtil.convertJavaNameToSQLName(fieldName);
                    String setterName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    // getting the type of the setter parameter, based on the field type
                    Class<?> setterParamType = clazz.getDeclaredField(fieldName).getType();
                    // obtain the setter method using the setter name and setter paramter type
                    Method setter = clazz.getMethod(setterName, setterParamType);
                    Object fieldValue = MapperUtil.getWhatever(rs, columnName, field.getType());
                    setter.invoke(newObject,fieldValue);
                }
                objectList.add(newObject);
            }
            connectionPool.releaseConnection(connection);
            return objectList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        connectionPool.releaseConnection(connection);
        return null;
    }

    public <T> List<T> getWhereEqual(Class<T>  clazz, String queryFieldName, Object queryFieldValue) {
        //System.out.println(clazz.getName());
        String[] words = clazz.getName().split("\\.");
        String tableName = words[words.length-1];
        String sql = "select * from " + MapperUtil.convertJavaNameToSQLName(tableName) + " where "
                + MapperUtil.convertJavaNameToSQLName(queryFieldName) + "= ?";
        Connection connection = connectionPool.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql);) {
            MapperUtil.setWhatever(ps, 1, queryFieldValue, queryFieldValue.getClass()); ///!!!! may need to get type of getDeclaredFields();
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            List<T> objectList = new ArrayList<>();
            Field[] fields = clazz.getDeclaredFields();
            while(rs.next()){
                T newObject = null;
                try {
                    newObject = clazz.newInstance();
                } catch (InstantiationException |IllegalAccessException e) {
                    e.printStackTrace();
                }
                for(Field field: fields) { //assumed order !!
                    String fieldName = field.getName();
                    String columnName = MapperUtil.convertJavaNameToSQLName(fieldName);
                    String setterName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    // getting the type of the setter parameter, based on the field type
                    Class<?> setterParamType = clazz.getDeclaredField(fieldName).getType();
                    // obtain the setter method using the setter name and setter paramter type
                    Method setter = clazz.getMethod(setterName, setterParamType);
                    Object fieldValue = MapperUtil.getWhatever(rs, columnName, field.getType());
                    setter.invoke(newObject,fieldValue);
                }
                objectList.add(newObject);
            }
            connectionPool.releaseConnection(connection);
            return objectList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        connectionPool.releaseConnection(connection);
        return null;
    }

    public <T> boolean deleteWhereEqual(Class<T>  clazz, String queryFieldName, Object queryFieldValue) {
        //System.out.println(clazz.getName());
        String[] words = clazz.getName().split("\\.");
        String tableName = words[words.length-1];
        String sql = "delete from " + MapperUtil.convertJavaNameToSQLName(tableName) + " where "
                + MapperUtil.convertJavaNameToSQLName(queryFieldName) + "= ?";
        Connection connection = connectionPool.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql);) {
            MapperUtil.setWhatever(ps, 1, queryFieldValue, queryFieldValue.getClass()); ///!!!! may need to get type of getDeclaredFields();
            System.out.println(ps);
            int success = ps.executeUpdate();
            if(success>0){
                connectionPool.releaseConnection(connection);
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        connectionPool.releaseConnection(connection);
        return false;
    }

    public <T> boolean updateWhereEqual(Class<T>  clazz, String queryFieldName, Object queryFieldValue, String updateFieldName, Object updateFieldValue) {
        //System.out.println(clazz.getName());
        String[] words = clazz.getName().split("\\.");
        String tableName = words[words.length-1];
        String sql = "update " + MapperUtil.convertJavaNameToSQLName(tableName) + " set " + MapperUtil.convertJavaNameToSQLName(updateFieldName) + "= ?"
                + " where " + MapperUtil.convertJavaNameToSQLName(queryFieldName) + "= ?";
        Connection connection = connectionPool.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql);) {
            MapperUtil.setWhatever(ps, 1, updateFieldValue, updateFieldValue.getClass());
            MapperUtil.setWhatever(ps, 2, queryFieldValue, queryFieldValue.getClass());
            System.out.println(ps);
            int success = ps.executeUpdate();
            if(success>0){
                connectionPool.releaseConnection(connection);
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        connectionPool.releaseConnection(connection);
        return false;
    }

}
