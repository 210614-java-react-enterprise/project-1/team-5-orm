package dev.model;

import java.util.Objects;

public class BankUser {
    private String email;
    private String password;

    public BankUser(String email, String password) {
        this.email = email;
        this.password = password;


    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public BankUser(){
        super();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankUser bankUser = (BankUser) o;
        return Objects.equals(email, bankUser.email) && Objects.equals(password, bankUser.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }
}
