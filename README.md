# Review Application Using Custom Object Relational Mapper

## Project Description

A servlet-based movie review application that uses a custom object relational mapping (ORM) framework, written in Java, which allows for a simplified and SQL-free interaction with a relational data source. This application uses the ORM to abstract away low-level JDBC, which allows it to easily query data. In this application, you can create users and movies, and reviews can be created by users for given movies. Users give both a written review and a review score, and the reviews for a given movie and the average score can be retrieved using the application.

## Technologies Used

* Java
* JDBC
* Maven
* GitLab
* PostgreSQL
* Postman
* DBBeaver
* Tomcat

## Features

* Custom Object Relational Mapper API
* Able to create new Users, Movies, and Reviews
* User can get all of the reviews for a given movie and get the average score
* User can efficiently get all movies due to caching 
* User can update review scores
* User can change movie titles

## Getting Started
   
* git clone https://gitlab.com/210614-java-react-enterprise/project-1/team-5-webapp.git
* git clone https://gitlab.com/210614-java-react-enterprise/project-1/team-5-orm.git
* orm: mvn clean package 
* webapp: run app with Tomcat

## Contributors

* Fuming Zhao
* Kalvin Miller
* Nick Bilotta

